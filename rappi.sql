-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 23-11-2020 a las 05:23:11
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `rappi`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `itinerary`
--

CREATE TABLE `itinerary` (
  `id` int(11) NOT NULL,
  `date_out` date NOT NULL,
  `date_in` date NOT NULL,
  `city_from` varchar(45) NOT NULL,
  `city_to` varchar(45) NOT NULL,
  `passenger` varchar(150) NOT NULL,
  `age` smallint(4) NOT NULL,
  `baggage` smallint(1) NOT NULL,
  `price` double(8,2) NOT NULL,
  `time_out` time NOT NULL,
  `time_in` time NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `itinerary`
--

INSERT INTO `itinerary` (`id`, `date_out`, `date_in`, `city_from`, `city_to`, `passenger`, `age`, `baggage`, `price`, `time_out`, `time_in`, `create_at`) VALUES
(1, '2020-11-21', '2020-11-22', 'Tlaxcala', 'Ciudad de Mexico', 'Felix Manuel Galicia Paredes', 41, 0, 95248.26, '17:00:00', '00:50:00', '2020-11-23 04:04:55');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `itinerary`
--
ALTER TABLE `itinerary`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `itinerary`
--
ALTER TABLE `itinerary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
