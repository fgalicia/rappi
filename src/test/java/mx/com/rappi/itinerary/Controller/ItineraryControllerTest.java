package mx.com.rappi.itinerary.Controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import mx.com.rappi.itinerary.controller.ItineraryController;
import mx.com.rappi.itinerary.model.dto.ItineraryDTO;
import mx.com.rappi.itinerary.response.ItineraryResponse;
import mx.com.rappi.itinerary.service.ItineraryService;

public class ItineraryControllerTest {

	private static final Long ID = 1L;
	private static final int OK = 200;
	
	public static final ItineraryDTO IT_RESPONSE = new ItineraryDTO();
	
	
	@Mock
	private ItineraryService itineraryService;
	
	@InjectMocks
	private ItineraryController itineraryController;
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		IT_RESPONSE.setDateOut(LocalDate.parse("2020-11-21"));
		IT_RESPONSE.setDateIn(LocalDate.parse("2020-11-22"));
		IT_RESPONSE.setCityFrom("Tlaxcala");
		IT_RESPONSE.setCityTo("Mexico");
		IT_RESPONSE.setPassenger("Felix Manuel Galicia Paredes");
		IT_RESPONSE.setAge(44);
		IT_RESPONSE.setBaggage(1);
		IT_RESPONSE.setPrice(56111.50);
		IT_RESPONSE.setTimeIn(LocalTime.parse("10:00:00"));
		IT_RESPONSE.setTimeOut(LocalTime.parse("23:00:00"));
		
		Mockito.when(itineraryService.findById(ID)).thenReturn(IT_RESPONSE);
	}
	
	@Test
	public void finByidTest() {
		ResponseEntity<ItineraryResponse> it = itineraryController.findById(ID);
		
		assertEquals(it.getBody().getCode(), OK);
		assertEquals(it.getBody().getData(), IT_RESPONSE);
	}
}
