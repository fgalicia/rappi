package mx.com.rappi.itinerary.service;

import mx.com.rappi.itinerary.model.dto.ItineraryDTO;
import mx.com.rappi.itinerary.model.entity.Itinerary;

public interface ItineraryService {

	public ItineraryDTO findById(Long id);
	public ItineraryDTO save(Itinerary entity);
}
