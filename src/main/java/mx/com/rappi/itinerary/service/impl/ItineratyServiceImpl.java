package mx.com.rappi.itinerary.service.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.rappi.itinerary.exception.NotFoundException;
import mx.com.rappi.itinerary.model.dto.ItineraryDTO;
import mx.com.rappi.itinerary.model.entity.Itinerary;
import mx.com.rappi.itinerary.repository.ItineraryRepository;
import mx.com.rappi.itinerary.service.ItineraryService;

@Service
public class ItineratyServiceImpl implements ItineraryService {

	@Autowired
	private ItineraryRepository itineraryRepository;
	@Autowired
	private static final ModelMapper mapper = new ModelMapper() ;
	
	
	@Override
	public ItineraryDTO findById(Long id) {
		Itinerary it = itineraryRepository.findById(id).orElse(null);
		if (it != null)
			return mapper.map(it, ItineraryDTO.class);
		else
			throw new NotFoundException("Itineray not found");
	}

	@Override
	public ItineraryDTO save(Itinerary entity) {
		return mapper.map(itineraryRepository.save(entity), ItineraryDTO.class);
	}

}
