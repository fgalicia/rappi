package mx.com.rappi.itinerary.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.com.rappi.itinerary.model.dto.ItineraryDTO;
import mx.com.rappi.itinerary.model.entity.Itinerary;
import mx.com.rappi.itinerary.response.ItineraryResponse;
import mx.com.rappi.itinerary.service.ItineraryService;

@RestController
@RequestMapping("itinerary/v1")
public class ItineraryController {

	@Autowired
	private ItineraryService itineraryService;
	
	private ItineraryResponse response = new ItineraryResponse();	
	
	@GetMapping("/{id}")
	public ResponseEntity <ItineraryResponse> findById(@PathVariable Long id){
		ItineraryDTO itinerary = itineraryService.findById(id);
		response.setData(itinerary);
		return new ResponseEntity<ItineraryResponse>(response, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<ItineraryDTO> save(@RequestBody Itinerary entity){
		ItineraryDTO itinerary = itineraryService.save(entity);
		if (itinerary.getId() != null) {
			return new ResponseEntity<ItineraryDTO>(itinerary, HttpStatus.CREATED);
		}
		else {
			return new ResponseEntity<ItineraryDTO>(new ItineraryDTO(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
			
	}
	
}
