package mx.com.rappi.itinerary.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.com.rappi.itinerary.model.entity.Itinerary;

public interface ItineraryRepository extends JpaRepository<Itinerary, Long> {

}
