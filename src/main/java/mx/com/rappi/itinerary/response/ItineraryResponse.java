package mx.com.rappi.itinerary.response;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItineraryResponse implements Serializable {

	private static final long serialVersionUID = 7233042539000554660L;
	
	private String message;
	private int code;
	private Object data;
	
	public ItineraryResponse() {
		this.message ="OK";
		this.code = 200;
	}

	public ItineraryResponse(String message, int code, Object data) {
		this.message ="OK";
		this.code = 200;
	}

}
