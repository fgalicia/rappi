package mx.com.rappi.itinerary.model.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@Entity
@Table(name ="itinerary")
public class Itinerary implements Serializable {

	private static final long serialVersionUID = -8283699995688694184L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "date_out")
	private LocalDate dateOut;
	
	@Column(name = "date_in")
	private LocalDate dateIn;
	
	@Column(name = "city_from")
	private String cityFrom;
	
	@Column(name = "city_to")
	private String cityTo;
	
	private String passenger;
	
	private Integer age;
	
	private Integer baggage;
	
	private Double price;
	
	@Column(name = "time_out")
	private LocalTime timeOut;
	
	@Column(name = "time_in") 
	private LocalTime timeIn;
	
	@Column(name = "create_at")
	private LocalDateTime createAt;
	
}
