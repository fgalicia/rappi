package mx.com.rappi.itinerary.model.dto;

import java.time.LocalDate;
import java.time.LocalTime;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class ItineraryDTO {
	private Long id;
	private LocalDate dateOut;
	private LocalDate dateIn;
	private String cityFrom;
	private String cityTo;
	private String passenger;
	private Integer age;
	private Integer baggage;
	private Double price;
	private LocalTime timeOut; 
	private LocalTime timeIn;
}
